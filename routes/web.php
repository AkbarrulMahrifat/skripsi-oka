<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\PenjualanController;
use App\Http\Controllers\PeramalanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login & root
Route::get('/', function () {
    return redirect('/login-page');
})->name('/');
Route::get('/login-page', [LoginController::class, 'index'])->name("login-page");
Route::post('/login', [LoginController::class, 'login'])->name("login");
Route::get('/logout', [LoginController::class, 'logout'])->name("logout");

// Pemilik
Route::group(['namespace' => 'Pemilik', 'prefix' => 'pemilik'], function () {
    Route::get('/', function () {
        return redirect('/pemilik/beranda');
    });

    Route::get('/beranda', [BerandaController::class, 'index'])->name('beranda');

    Route::get('/produk', [ProdukController::class, 'index'])->name('produk');
    Route::post('/tambah_produk', [ProdukController::class, 'store'])->name('tambah produk');
    Route::put('/ubah_produk', [ProdukController::class, 'update'])->name('ubah produk');
    Route::delete('/hapus_produk/{id}', [ProdukController::class, 'delete'])->name('hapus produk');
    Route::get('/stok', [ProdukController::class, 'stok'])->name('stok');
    Route::put('/ubah_stok', [ProdukController::class, 'updateStok'])->name('ubah stok');

    Route::get('/penjualan', [PenjualanController::class, 'index'])->name('penjualan');
    Route::get('/detail_penjualan', [PenjualanController::class, 'show'])->name('detail penjualan');
    Route::post('/tambah_penjualan', [PenjualanController::class, 'store'])->name('tambah penjualan');
    Route::delete('/hapus_penjualan/{id}', [PenjualanController::class, 'delete'])->name('hapus penjualan');

    Route::get('/peramalan', [PeramalanController::class, 'index'])->name('peramalan');
    Route::get('/hitung_peramalan', [PeramalanController::class, 'hitungPeramalan'])->name('hitung peramalan');
    Route::post('/simpan_peramalan', [PeramalanController::class, 'store'])->name('simpan peramalan');
    Route::delete('/hapus_peramalan/{id}', [PeramalanController::class, 'delete'])->name('hapus peramalan');

    Route::get('/users', [UsersController::class, 'index'])->name('users');
    Route::post('/tambah_users', [UsersController::class, 'store'])->name('tambah users');
    Route::put('/ubah_users', [UsersController::class, 'update'])->name('ubah users');
    Route::delete('/hapus_users/{id}', [UsersController::class, 'delete'])->name('hapus users');
});

// Pegawai
Route::group(['namespace' => 'Pegawai', 'prefix' => 'pegawai'], function () {
    Route::get('/', function () {
        return redirect('/pegawai/beranda');
    });

    Route::get('/beranda', [BerandaController::class, 'index'])->name('beranda');

    Route::get('/produk', [ProdukController::class, 'index'])->name('produk');
    Route::post('/tambah_produk', [ProdukController::class, 'store'])->name('tambah produk');
    Route::put('/ubah_produk', [ProdukController::class, 'update'])->name('ubah produk');
    Route::delete('/hapus_produk/{id}', [ProdukController::class, 'delete'])->name('hapus produk');
    Route::get('/stok', [ProdukController::class, 'stok'])->name('stok');
    Route::put('/ubah_stok', [ProdukController::class, 'updateStok'])->name('ubah stok');

    Route::get('/penjualan', [PenjualanController::class, 'index'])->name('penjualan');
    Route::get('/detail_penjualan', [PenjualanController::class, 'show'])->name('detail penjualan');
    Route::post('/tambah_penjualan', [PenjualanController::class, 'store'])->name('tambah penjualan');
    Route::delete('/hapus_penjualan/{id}', [PenjualanController::class, 'delete'])->name('hapus penjualan');

    Route::get('/peramalan', [PeramalanController::class, 'index'])->name('peramalan');
    Route::get('/hitung_peramalan', [PeramalanController::class, 'hitungPeramalan'])->name('hitung peramalan');
    Route::post('/simpan_peramalan', [PeramalanController::class, 'store'])->name('simpan peramalan');
    Route::delete('/hapus_peramalan/{id}', [PeramalanController::class, 'delete'])->name('hapus peramalan');

    Route::get('/users', [UsersController::class, 'index'])->name('users');
    Route::post('/tambah_users', [UsersController::class, 'store'])->name('tambah users');
    Route::put('/ubah_users', [UsersController::class, 'update'])->name('ubah users');
    Route::delete('/hapus_users/{id}', [UsersController::class, 'delete'])->name('hapus users');
});