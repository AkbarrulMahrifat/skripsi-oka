@extends('layouts.master')

@section('title', 'Stok')
@section('title-2', 'Stok')
@section('title-3', 'Stok')

@section('content')
    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Data Stok Produk</h6>
                </div>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush table-hover" id="dataTable">
                        <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Nama Produk</th>
                            <th>Foto Produk</th>
                            <th>Stok</th>
                            <th>Rekomendasi</th>
                            @if(session()->get("role") == "pegawai")
                                <th>Aksi</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @php($no = 1)
                        @foreach($produk as $p)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $p->nama }}</td>
                                <td>
                                    <img src="{{ asset("assets/foto_produk/".$p->foto) }}"
                                         class="img-thumbnail image-preview" style="width: 100px"
                                         data-url="{{ asset("assets/foto_produk/".$p->foto) }}" data-name="{{ $p->nama }}">
                                </td>
                                <td>{{ $p->stok }}</td>
                                <td>
                                    Rekomendasi : {{ $p->rekomendasi }}
                                    <br>
                                    Periode : {{ $p->periode_rekomendasi }}
                                </td>
                                @if(session()->get("role") == "pegawai")
                                    <td>
                                        <button class="btn btn-sm btn-success stok" title="Tambah Stok"
                                                data-id="{{ $p->id }}" data-nama="{{ $p->nama }}" data-stok="{{ $p->stok }}" data-tipe="+">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger stok" title="Kurangi Stok"
                                                data-id="{{ $p->id }}" data-nama="{{ $p->nama }}" data-stok="{{ $p->stok }}" data-tipe="-">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal --}}
    <div class="modal fade" id="stok-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('ubah stok') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PUT">
                    <input type="hidden" id="stok-id" name="id">
                    <input type="hidden" id="stok-tipe" name="tipe">
                    <input type="hidden" id="stok-awal" name="stok_awal">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><span id="stok-title"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="stok-stok">Stok</label>
                            <input type="text" class="form-control" id="stok-stok" name="stok" placeholder="Stok produk" readonly>
                        </div>
                        <div class="form-group">
                            <label for="stok-input">Input</label>
                            <input type="number" min="0" class="form-control" id="stok-input" name="input" placeholder="Input" value="0" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $('.stok').on('click',function(e){
            $('#stok-input').val(0);
            var tipe = $(this).data("tipe");

            if (tipe == "-" && parseInt($(this).data("stok")) <= 0) {
                notif("error", "Stok habis, tidak dapat dikurangi");
                return false;
            }

            var title = " Stok " + $(this).data("nama");
            if (tipe == "-") {
                title = "Kurangi" + title;
            } else {
                title = "Tambah" + title;
            }
            $("#stok-id").val($(this).data("id"));
            $("#stok-nama").val($(this).data("nama"));
            $("#stok-tipe").val($(this).data("tipe"));
            $("#stok-stok").val(parseInt($(this).data("stok")));
            $("#stok-awal").val(parseInt($(this).data("stok")));
            $("#stok-title").html(title);
            $("#stok-modal").modal("show");
        });

        $('#stok-input').on('input',function(e){
            var input = parseInt($('#stok-input').val());
            var stok_awal = parseInt($('#stok-awal').val());
            var tipe = $('#stok-tipe').val();
            $('#stok-input').val(input);
            var stok = 0;
            if (tipe == "+") {
                stok = stok_awal + input;
            }
            if (tipe == "-") {
                stok = stok_awal - input;
            }
            $('#stok-stok').val(stok);
            // $("#stok-id").val($(this).data("id"));
            // $("#stok-nama").val($(this).data("nama"));
            // $("#stok-stok").val($(this).data("deskripsi"));
            // $("#stok-title").val(parseInt($(this).data("harga")));
            // $("#stok-modal").modal("show");
        });
    </script>
@endpush