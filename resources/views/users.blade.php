@extends('layouts.master')

@section('title', 'User')
@section('title-2', 'User')
@section('title-3', 'User')

@section('content')
    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Data User</h6>

                    <button class="btn btn-sm btn-success btn-icon-split" title="Tambah User" id="add">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah</span>
                    </button>
                </div>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush table-hover" id="dataTable">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Username</th>
                                <th>Nama</th>
                                <th>Foto</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php($no = 1)
                        @foreach($users as $i)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $i->username }}</td>
                                <td>{{ $i->nama }}</td>
                                <td>
                                    <img src="{{ asset("assets/foto_profil/".$i->foto) }}"
                                         class="img-thumbnail image-preview" style="width: 100px"
                                         data-url="{{ asset("assets/foto_profil/".$i->foto) }}" data-name="{{ $i->nama }}">
                                </td>
                                <td>{{ $i->role }}</td>
                                <td>{{ $i->status }}</td>
                                <td>
                                    <button class="btn btn-sm btn-primary edit" id="edit" title="Ubah User"
                                            data-id="{{ $i->id }}"
                                            data-nama="{{ $i->nama }}"
                                            data-username="{{ $i->username }}"
                                            data-foto="{{ $i->foto }}"
                                            data-role="{{ $i->role }}"
                                            data-status="{{ $i->status }}"
                                            data-is_active="{{ $i->is_active }}"
                                    >
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button class="btn btn-sm btn-danger delete" id="delete" title="HapusUser"
                                            onclick="deleteModal('{{ $i->nama }}', '{{ route('hapus users', ["id"=>$i->id]) }}')">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal --}}
    <div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('tambah users') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="add-nama">Nama User</label>
                            <input type="text" class="form-control" id="add-nama" name="nama" placeholder="Nama User" required>
                        </div>
                        <div class="form-group">
                            <label for="add-username">Username</label>
                            <input type="text" class="form-control" id="add-username" name="username" placeholder="Username User" required>
                        </div>
                        <div class="form-group">
                            <label for="add-password">Password</label>
                            <input type="password" class="form-control" id="add-password" name="password" placeholder="Password User" required>
                        </div>
                        <div class="form-group">
                            <label for="add-role">Role</label>
                            <select class="form-control" id="add-role" name="role" required>
                                <option value="">Pilih Role</option>
                                <option value="pemilik">Pemilik</option>
                                <option value="pegawai">Pegawai</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="add-status">Status</label>
                            <select class="form-control" id="add-status" name="is_active" required>
                                <option value="y">Aktif</option>
                                <option value="n">Non Aktif</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="add-foto">Foto User</label>
                            <input type="file" class="form-control-file" id="add-foto" name="foto">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('ubah users') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PUT">
                    <input name="id" type="hidden" id="edit-id">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ubah User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="edit-nama">Nama User</label>
                            <input type="text" class="form-control" id="edit-nama" name="nama" placeholder="Nama User" required>
                        </div>
                        <div class="form-group">
                            <label for="edit-username">Username</label>
                            <input type="text" class="form-control" id="edit-username" name="username" placeholder="Username User" required>
                        </div>
                        <div class="form-group">
                            <label for="edit-role">Role</label>
                            <select class="form-control" id="edit-role" name="role" required>
                                <option value="">Pilih Role</option>
                                <option value="pemilik">Pemilik</option>
                                <option value="pegawai">Pegawai</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="edit-status">Status</label>
                            <select class="form-control" id="edit-status" name="is_active" required>
                                <option value="y">Aktif</option>
                                <option value="n">Non Aktif</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="edit-password">Password Baru (Optional)</label>
                            <input type="password" class="form-control" id="edit-password" name="password" placeholder="Input jika ingin mengganti Password Baru">
                        </div>
                        <div class="form-group">
                            <label for="edit-foto">Foto User</label>
                            <input type="file" class="form-control-file" id="edit-foto" name="foto">
                            <small id="edit-foto-old" class="form-text text-muted"></small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $('#add').on('click',function(e){
            $("#add-modal").modal("show");
        });

        $('.edit').on('click',function(e){
            $("#edit-id").val($(this).data("id"));
            $("#edit-nama").val($(this).data("nama"));
            $("#edit-username").val($(this).data("username"));
            $("#edit-password").val("");
            $("#edit-role").val($(this).data("role")).change();
            $("#edit-status").val($(this).data("is_active")).change();
            $("#edit-foto-old").html($(this).data("foto"));
            $("#edit-modal").modal("show");
        });
    </script>
@endpush