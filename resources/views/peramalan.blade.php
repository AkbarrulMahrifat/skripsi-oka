@extends('layouts.master')

@section('title', 'Peramalan')
@section('title-2', 'Peramalan')
@section('title-3', 'Peramalan')

@section('content')
    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Hitung Peramalan</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('hitung peramalan') }}" method="get" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="add-nama">Produk</label>
                                <select id="produk_id" class="form-control" name="produk_id" required>
                                    <option value="">Pilih Produk</option>
                                    @foreach($produk as $p)
                                        <option value="{{ $p->id }}">{{ $p->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="add-deskripsi">Periode Peramalan</label>
                                <input type="text" class="form-control bulan" id="periode-peramalan" name="periode_peramalan"
                                       placeholder="Tanggal Peramalan" required>
                            </div>
                        </div>
                        <button type="button" class="btn btn-outline-danger">Reset</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Daftar Peramalan</h6>
                </div>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush table-hover" id="dataTable">
                        <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Nama Produk</th>
                            <th>Periode</th>
                            <th>Beta</th>
                            <th>Hasil Peramalan</th>
                            <th>MAPE (%)</th>
                            <th>Rekomendasi</th>
                            <th>Dibuat Tanggal</th>
                            <th>Dibuat Oleh</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($no = 1)
                        @foreach($peramalan as $h)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $h->produk->nama }}</td>
                                <td>{{ $h->periode }}</td>
                                <td>{{ $h->beta }}</td>
                                <td>{{ $h->hasil_peramalan }}</td>
                                <td>{{ $h->mape }}</td>
                                <td>{{ $h->rekomendasi }}</td>
                                <td>{{ $h->tanggal_dibuat }}</td>
                                <td>{{ $h->users->nama }}</td>
                                <td>
                                    <button class="btn btn-sm btn-danger delete" id="delete" title="Hapus Peramalan"
                                            onclick="deleteModal('{{ "Peramalan " . $h->produk->nama . " Periode " . $h->periode }}', '{{ route('hapus peramalan', ["id"=>$h->id]) }}')">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
    </script>
@endpush