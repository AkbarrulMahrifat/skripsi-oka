<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>FDL Store - @yield('title')</title>
<link href="{{ asset('assets/img/logo/logo-fdl.jpg') }}" rel="shortcut icon" type="image/x-icon">
<link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/ruang-admin.min.css') }}" rel="stylesheet">
<link href="{{asset('assets/vendor/toastr/toastr.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
{{-- Select2 --}}
<link href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css">
{{-- Bootstrap DatePicker --}}
<link href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" >
{{-- Bootstrap Touchspin --}}
<link href="{{ asset('assets/vendor/bootstrap-touchspin/css/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" >
{{-- ClockPicker --}}
<link href="{{ asset('assets/vendor/clock-picker/clockpicker.css') }}" rel="stylesheet">
{{-- Bootstrap Select --}}
<link href="{{ asset('assets/vendor/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" >
@stack('css')