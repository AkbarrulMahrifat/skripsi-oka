<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('assets/js/ruang-admin.min.js') }}"></script>
<script src="{{asset('assets/vendor/toastr/toastr.min.js')}}"></script>
<script src="{{ asset('assets/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
{{-- Select2 --}}
<script src="{{ asset('assets/vendor/select2/dist/js/select2.min.js') }}"></script>
{{-- Bootstrap Datepicker --}}
<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
{{-- Bootstrap Touchspin --}}
<script src="{{ asset('assets/vendor/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js') }}"></script>
{{-- ClockPicker --}}
<script src="{{ asset('assets/vendor/clock-picker/clockpicker.js') }}"></script>
{{-- Bootstrap Select --}}
<script src="{{ asset('assets/vendor/bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#dataTable').DataTable(); // ID From dataTable with Hover

        $('.image-preview').click(function(e){
            $("#modal-image-url").attr("src", $(this).data("url"));
            $("#modal-image-name").html($(this).data("name"));
            $("#imageModal").modal("show");
        });

        // $('.delete').click(function(e){
        //     $("#delete-modal-form").attr("action", $(this).data("url"));
        //     $("#delete-modal-title").html("Hapus Data " + $(this).data("name"));
        //     $("#delete-modal-name").html($(this).data("name"));
        //     $("#deleteModal").modal("show");
        // });

        $('.select2-single').select2();

        // Select2 Single  with Placeholder
        $('.select2-single-placeholder').select2({
            placeholder: "Select a Province",
            allowClear: true
        });

        // Select2 Multiple
        $('.select2-multiple').select2();

        // Bootstrap Date Picker
        $('#simple-date1 .input-group.date').datepicker({
            format: 'dd/mm/yyyy',
            todayBtn: 'linked',
            todayHighlight: true,
            autoclose: true,
        });

        $('#simple-date2 .input-group.date').datepicker({
            startView: 1,
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
            todayBtn: 'linked',
        });

        $('#simple-date3 .input-group.date').datepicker({
            startView: 2,
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
            todayBtn: 'linked',
        });

        $('#simple-date4 .input-daterange').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
            todayBtn: 'linked',
        });

        $('.bulan').datepicker({
            changeMonth: true,
            changeYear: true,
            startView: "months",
            minViewMode: "months",
            format: 'yyyy-mm',
            todayBtn: 'linked',
            todayHighlight: true,
            autoclose: true,
        });

        $('.tanggal').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: 'linked',
            todayHighlight: true,
            autoclose: true,
        });

        $('.waktu').clockpicker({
            donetext: 'Done',
            autoclose: true
        });

        // TouchSpin
        $('#touchSpin1').TouchSpin({
            min: 0,
            max: 100,
            boostat: 5,
            maxboostedstep: 10,
            initval: 0
        });

        $('#touchSpin2').TouchSpin({
            min:0,
            max: 100,
            decimals: 2,
            step: 0.1,
            postfix: '%',
            initval: 0,
            boostat: 5,
            maxboostedstep: 10
        });

        $('#touchSpin3').TouchSpin({
            min: 0,
            max: 100,
            initval: 0,
            boostat: 5,
            maxboostedstep: 10,
            verticalbuttons: true,
        });

        $('#clockPicker1').clockpicker({
            donetext: 'Done'
        });

        $('#clockPicker2').clockpicker({
            autoclose: true
        });

        let input = $('#clockPicker3').clockpicker({
            autoclose: true,
            'default': 'now',
            placement: 'top',
            align: 'left',
        });

        $('#check-minutes').click(function(e){
            e.stopPropagation();
            input.clockpicker('show').clockpicker('toggleView', 'minutes');
        });
    });

    toastr.options = {
        "closeButton" : true
    }
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}", "Success!");
    @endif
    @if(Session::has('error'))
    toastr.error("{{ Session::get('error') }}", "Error!");
    @endif

    function notif(tipe, text) {
        if (tipe == "error") {
            toastr.error(text, "Error!");
        } else {
            toastr.success(text, "Success!");
        }
    }

    function deleteModal(name, url) {
        $("#delete-modal-form").attr("action", url);
        $("#delete-modal-title").html("Hapus Data " + name);
        $("#delete-modal-name").html(name);
        $("#deleteModal").modal("show");
    }
</script>
@stack('js')