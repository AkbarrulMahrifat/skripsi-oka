<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('/') }}">
        <div class="sidebar-brand-icon">
            <img src="{{ asset('assets/img/logo/logo-fdl.jpg') }}">
        </div>
        <div class="sidebar-brand-text mx-3">FDL Store</div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item{{ request()->routeIs('beranda') ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('beranda') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Beranda</span>
        </a>
    </li>
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Produk
    </div>
    @if(session()->get("role") == "pemilik")
        <li class="nav-item{{ request()->routeIs('produk') ? ' active' : ''}}">
            <a class="nav-link" href="{{ route('produk') }}">
                <i class="fas fa-fw fa-book"></i>
                <span>Produk</span>
            </a>
        </li>
    @endif
    <li class="nav-item{{ request()->routeIs('stok') ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('stok') }}">
            <i class="fas fa-fw fa-archive"></i>
            <span>Stok</span>
        </a>
    </li>
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Penjualan
    </div>
    <li class="nav-item{{ request()->routeIs('penjualan') ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('penjualan') }}">
            <i class="fas fa-fw fa-money-bill"></i>
            <span>Penjualan</span>
        </a>
    </li>
    @if(session()->get("role") == "pegawai")
        <li class="nav-item{{ request()->routeIs('peramalan') ?  ' active' : ''}} {{ request()->routeIs('hitung peramalan') ?  ' active' : ''}}">
            <a class="nav-link" href="{{ route('peramalan') }}">
                <i class="fas fa-fw fa-chart-line"></i>
                <span>Peramalan</span>
            </a>
        </li>
    @endif
    @if(session()->get("role") == "pemilik")
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            User
        </div>
        <li class="nav-item{{ request()->routeIs('users') ? ' active' : ''}}">
            <a class="nav-link" href="{{ route('users') }}">
                <i class="fas fa-fw fa-users"></i>
                <span>User</span>
            </a>
        </li>
    @endif
</ul>