@extends('layouts.master')

@section('title', 'Hasil Peramalan')
@section('title-2', 'Hasil Peramalan')
@section('title-3', 'Hasil Peramalan')

@section('content')
    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Hasil Peramalan "{{ $produk->nama }}" Periode "{{ date("M Y", strtotime($to)) }}"</h6>
                </div>
                <div class="card-body row">
                    <div class="table-responsive col-md-6">
                        <table class="table align-items-center table-flush table-hover">
                            <thead class="thead-light">
                            <tr>
                                <th>Beta</th>
                                <th>Hasil</th>
                                <th>MAPE</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($hasil as $key => $h)
                                @php($classBgSuccess="")
                                @if($key == $indexMapeTerkecil)
                                    @php($classBgSuccess="bg-success text-light")
                                @endif
                                <tr class="{{$classBgSuccess}}">
                                    <td>{{ $h[$indexPeramalan]["beta"] }}</td>
                                    <td>{{ round($h[$indexPeramalan]["hasil"], 2) }}</td>
                                    <td>{{ round($h[$indexPeramalan]["MAPE"], 2) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class=" col-md-6">
                        <h6 class="m-0 font-weight-bold text-primary">Kesimpulan :</h6>
                        <p>
                            Berdasarkan perhitungan peramalan menggunakan metode <i>ARRSES</i>,
                            beta terbaik adalah <b>{{ $bestBetaIndex }}</b>,
                            dengan hasil peramalan <b>{{ round($hasil[$indexMapeTerkecil][$indexPeramalan]["hasil"], 2) }}</b>
                            dan tingkat error sebesar <b>{{ round($hasil[$indexMapeTerkecil][$indexPeramalan]["MAPE"], 2) }} %</b>.
                        </p>
                        <p>
                            Hasil peramalan penjualan baju <b>{{ $produk->nama }}</b>
                            untuk periode <b>{{ date("M Y", strtotime($to)) }}</b>
                            adalah <b>{{ round($hasil[$indexMapeTerkecil][$indexPeramalan]["hasil"]) }}</b>.
                        </p>
                        <hr>
                        <form class="row" style="margin: 0" method="post" action="{{ route('simpan peramalan') }}">
                            @csrf
                            <input type="hidden" value="{{ $produk_id }}" name="produk_id" required/>
                            <input type="hidden" value="{{ $to }}" name="periode" required/>
                            <input type="hidden" value="{{ $bestBetaIndex }}" name="beta" required/>
                            <input type="hidden" value="{{ round($hasil[$indexMapeTerkecil][$indexPeramalan]["hasil"], 2) }}" name="hasil_peramalan" required/>
                            <input type="hidden" value="{{ round($hasil[$indexMapeTerkecil][$indexPeramalan]["MAPE"], 2) }}" name="mape" required/>
                            <input type="hidden" value="{{ round($hasil[$indexMapeTerkecil][$indexPeramalan]["hasil"]) }}" name="rekomendasi" required/>
                            <div class="form-group">
                                <div class="input-group">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a class="btn btn-outline-danger ml-2" href="{{ route('peramalan') }}">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--Detail Perhitungan--}}
    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Detail Perhitungan Tiap Beta</h6>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div id="accordion">
                        @foreach ($beta as $key => $b)
                        <div class="card card-primary mb-2">
                            <div class="card-header">
                                <h4 class="card-title w-100" style="margin: 0">
                                    <a class="d-block w-100" data-toggle="collapse" href="#collapse{{ $key }}">
                                        <h6 class="m-0 font-weight-bold text-dark">
                                            Detail Perhitungan Beta {{ $b }} <i class="fa fa-chevron-down"></i>
                                        </h6>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{ $key }}" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table align-items-center table-flush table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>Periode</th>
                                                <th>Aktual</th>
                                                <th>Hasil Prediksi</th>
                                                <th>e</th>
                                                <th>E</th>
                                                <th>EA</th>
                                                <th>Alpha</th>
                                                <th>Beta</th>
                                                <th>PE (%)</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($hasil[$key] as $key2 => $i)
                                                @php($mape[$key] = $i['MAPE'])
                                                <tr>
                                                    <td>{{ date("M Y", strtotime($i['tanggal'])) }}</td>
                                                    <td>{{ $i['aktual'] }}</td>
                                                    <td>{{ round($i['hasil'], 2) }}</td>
                                                    <td>{{ round($i['ek'], 2) }}</td>
                                                    <td>{{ round($i['Eb'], 2) }}</td>
                                                    <td>{{ round($i['EA'], 2) }}</td>
                                                    <td>{{ round($i['alpha'], 2) }}</td>
                                                    <td>{{ round($i['beta'], 2) }}</td>
                                                    <td>{{ round($i['PE'], 2) }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="8" style="text-align:right">MAPE</td>
                                                <td>{{ round($mape[$key], 2) }}</td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@endsection

@push('js')
    <script>

    </script>
@endpush