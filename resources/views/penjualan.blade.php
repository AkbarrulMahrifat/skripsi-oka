@extends('layouts.master')

@section('title', 'Penjualan')
@section('title-2', 'Penjualan')
@section('title-3', 'Penjualan')

@section('content')
    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Data Penjualan</h6>

                    @if(session()->get("role") == "pegawai")
                        <button class="btn btn-sm btn-success btn-icon-split" title="Tambah Produk" id="add">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                            <span class="text">Tambah</span>
                        </button>
                    @endif
                </div>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush table-hover" id="dataTable">
                        <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Tanggal</th>
                            <th>Total Pesanan</th>
                            <th>Dibuat Oleh</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($no = 1)
                        @foreach($penjualan as $p)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $p->tanggal }}</td>
                                <td>{{ $p->total }}</td>
                                <td>{{ $p->users->nama }}</td>
                                <td>
                                    <button class="btn btn-sm btn-primary" title="Detail Penjualan" onclick="detail({{ $p->id }})">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                    @if(session()->get("role") == "pegawai")
                                        <button class="btn btn-sm btn-danger delete" id="delete" title="Hapus Penjualan"
                                                onclick="deleteModal('Pesanan #{{ $p->id }}', '{{ route('hapus penjualan', ["id"=>$p->id]) }}')">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal --}}
    <div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 800px">
            <div class="modal-content">
                <form action="{{ route('tambah penjualan') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Penjualan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="add-tanggal" class="col-sm-3 col-form-label">Tanggal Pesanan</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control tanggal" id="add-tanggal" name="tanggal" placeholder="Tanggal Pesanan"
                                       value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" required>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control waktu" id="add-waktu" name="waktu" placeholder="Waktu Pesanan"
                                       value="{{ \Carbon\Carbon::now()->format('H:i') }}" required>
                            </div>
                        </div>

                        <br>

                        {{--tabel item--}}
                        <div class="table-responsive">
                            <label>Pilih Produk</label>
                            <button type="button" class="btn btn-sm btn-primary btn-icon-split float-md-right" title="Tambah Item" id="add-item">
                                <span class="icon text-white-50">
                                    <i class="fas fa-plus"></i>
                                </span>
                                <span class="text">Tambah Item</span>
                            </button>
                            <table class="table align-items-center table-flush">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Produk</th>
                                    <th>Qty</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody id="item-list">
                                <input id="index" type="hidden" value="0">
                                <tr id="item-0">
                                    <td>1</td>
                                    <td>
                                        <select id="add-produk_id-0" class="form-control form-control-sm" name="items[0][produk_id]" required>
                                            <option value="">Pilih Produk</option>
                                            @foreach($produk as $p)
                                                <option value="{{ $p->id }}">{{ $p->nama }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input id="add-qty-0" class="form-control form-control-sm" type="number" name="items[0][qty]" required></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="detail-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 800px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Penjualan #<span id="detail-id"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <p class="col-sm-3">Tanggal Pesanan</p>
                        <p class="col-sm-5">: <span id="detail-tanggal"></span></p>
                    </div>
                    <div class="form-group row">
                        <p class="col-sm-3">Dibuat Oleh</p>
                        <p class="col-sm-5">: <span id="detail-dibuat-oleh"></span></p>
                    </div>
                    <br>
                    {{--tabel item--}}
                    <div class="table-responsive">
                        <label>Daftar Produk</label>
                        <table class="table align-items-center table-flush">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Produk</th>
                                <th>Qty</th>
                            </tr>
                            </thead>
                            <tbody id="detail-item-list">
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="2" style="text-align: right">Total</th>
                                <th>
                                    <span id="detail-total-qty"></span>
                                </th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function getProdukSelect(n=0) {
            var html = '<select class="form-control form-control-sm" name="items[';
            html += n;
            html += '][produk_id]" required>';
            html += '<option value="">Pilih Produk</option>';
            @foreach($produk as $p)
                html += '<option value="{{ $p->id }}">{{ $p->nama }}</option>';
            @endforeach
                html += '</select>';

            return html;
        }

        function removeItem(n) {
            $("#item-"+n.toString()).remove();
        }

        $('#add').on('click',function(e){
            $("#add-modal").modal("show");
        });

        $('#add-item').on('click',function(e){
            var index = parseInt($('#index').val());
            var n = index+1;
            var number = n+1;
            var select_produk = getProdukSelect(n);
            var html = '<tr id="item-' + n.toString() + '"><td>'
                + number.toString() +
                '</td><td>'
                + select_produk +
                '</td><td><input id="add-qty-'
                + n.toString() +
                '" class="form-control form-control-sm" type="number" name="items['
                + n.toString() +
                '][qty]" required></td><td><button type="button" class="btn btn-sm btn-danger delete_item" data-index="'
                + n.toString() +
                '" onclick="removeItem('
                + n +
                ')"><i class="fas fa-trash"></i></button></td></tr>';
            $('#item-list').append(html)
        });

        function detail(id) {
            $.ajax({
                url: "{{ route('detail penjualan') }}",
                data: {
                    "id" : id
                },
                type: 'GET',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    var users = data.users;
                    var items = data.penjualan_detail;
                    $("#detail-id").html(data.id);
                    $("#detail-tanggal").html(data.tanggal);
                    $("#detail-dibuat-oleh").html(users.nama);
                    $("#detail-total-qty").html(data.total);
                    var html = '';
                    for (i = 0; i < items.length; i++) {
                        var number = i+1;
                        var produk = items[i].produk;
                        html += '<tr id="item-'
                            + i +
                            '"><td>'
                            + number +
                            '</td><td>'
                            + produk.nama +
                            '</td><td>'
                            + items[i].qty +
                            '</td></tr>';
                    }
                    $("#detail-item-list").html(html);
                    $("#detail-modal").modal("show");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("NO");
                }
            });
        }
    </script>
@endpush