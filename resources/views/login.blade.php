<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="{{asset('assets/img/logo/logo.png')}}" rel="icon">
    <title>RuangAdmin - Login</title>
    <link href="{{asset('assets/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/ruang-admin.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/toastr/toastr.min.css')}}" rel="stylesheet" type="text/css">

</head>

<body class="bg-gradient-login">
<!-- LoginController Content -->
<div class="container-login">
    <div class="row justify-content-center">
        <div class="col-xl-6 col-lg-12 col-md-9">
            <div class="card shadow-sm my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="login-form">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Login</h1>
                                </div>
                                <form class="user" action="{{route('login')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <input type="text" name="username" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Username" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control" id="exampleInputPassword" placeholder="Password" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block">Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- LoginController Content -->
<script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('assets/vendor/toastr/toastr.min.js')}}"></script>
<script>
    toastr.options = {
        "closeButton" : true
    }
    @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}", "Success!");
    @endif
    @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}", "Error!");
    @endif
</script>
<script src="{{asset('assets/js/ruang-admin.min.js')}}"></script>
</body>

</html>