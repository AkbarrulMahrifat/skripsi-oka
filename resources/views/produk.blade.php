@extends('layouts.master')

@section('title', 'Produk')
@section('title-2', 'Produk')
@section('title-3', 'Produk')

@section('content')
    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Data Produk</h6>

                    <button class="btn btn-sm btn-success btn-icon-split" title="Tambah Produk" id="add">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah</span>
                    </button>
                </div>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush table-hover" id="dataTable">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Deskripsi</th>
                                <th>Foto</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php($no = 1)
                        @foreach($produk as $p)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $p->nama }}</td>
                                <td>{{ $p->deskripsi }}</td>
                                <td>
                                    <img src="{{ asset("assets/foto_produk/".$p->foto) }}"
                                         class="img-thumbnail image-preview" style="width: 100px"
                                         data-url="{{ asset("assets/foto_produk/".$p->foto) }}" data-name="{{ $p->nama }}">
                                </td>
                                <td>
                                    <button class="btn btn-sm btn-primary edit" id="edit" title="Ubah Produk"
                                            data-id="{{ $p->id }}"
                                            data-nama="{{ $p->nama }}"
                                            data-deskripsi="{{ $p->deskripsi }}"
                                            data-foto="{{ $p->foto }}"
                                    >
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button class="btn btn-sm btn-danger delete" id="delete" title="HapusProduk"
                                            onclick="deleteModal('{{ $p->nama }}', '{{ route('hapus produk', ["id"=>$p->id]) }}')">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal --}}
    <div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('tambah produk') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Produk</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="add-nama">Nama Produk</label>
                            <input type="text" class="form-control" id="add-nama" name="nama" placeholder="Nama produk" required>
                        </div>
                        <div class="form-group">
                            <label for="add-deskripsi">Deskripsi</label>
                            <textarea class="form-control" id="add-deskripsi" name="deskripsi" placeholder="Deskripsi produk"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="add-foto">Foto Produk</label>
                            <input type="file" class="form-control-file" id="add-foto" name="foto">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('ubah produk') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PUT">
                    <input name="id" type="hidden" id="edit-id">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ubah Produk</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="edit-nama">Nama Produk</label>
                            <input type="text" class="form-control" id="edit-nama" name="nama" placeholder="Nama produk" required>
                        </div>
                        <div class="form-group">
                            <label for="add-deskripsi">Deskripsi</label>
                            <textarea class="form-control" id="edit-deskripsi" name="deskripsi" placeholder="Deskripsi produk"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="edit-foto">Foto Produk</label>
                            <input type="file" class="form-control-file" id="edit-foto" name="foto">
                            <small id="edit-foto-old" class="form-text text-muted"></small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $('#add').on('click',function(e){
            $("#add-modal").modal("show");
        });

        $('.edit').on('click',function(e){
            $("#edit-id").val($(this).data("id"));
            $("#edit-nama").val($(this).data("nama"));
            $("#edit-deskripsi").val($(this).data("deskripsi"));
            $("#edit-foto-old").html($(this).data("foto"));
            $("#edit-modal").modal("show");
        });
    </script>
@endpush