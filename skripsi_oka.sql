-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2021 at 07:59 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_oka`
--

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `tanggal` timestamp NULL DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id`, `tanggal`, `total`, `user_id`) VALUES
(1, '2020-02-10 02:00:00', 105, 2),
(2, '2020-03-11 02:00:00', 97, 2),
(3, '2020-04-12 02:00:00', 96, 2),
(4, '2020-05-13 02:00:00', 85, 2),
(5, '2020-06-14 02:00:00', 93, 2),
(6, '2020-07-15 02:00:00', 112, 2),
(7, '2020-08-16 02:00:00', 103, 2),
(8, '2020-09-17 02:00:00', 118, 2),
(9, '2020-10-18 02:00:00', 120, 2),
(10, '2020-11-19 02:00:00', 105, 2),
(11, '2020-12-20 02:00:00', 93, 2),
(12, '2021-01-21 02:00:00', 97, 2);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id` int(11) NOT NULL,
  `penjualan_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `qty` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id`, `penjualan_id`, `produk_id`, `qty`) VALUES
(1, 1, 1, 68),
(2, 2, 1, 56),
(3, 3, 1, 60),
(4, 4, 1, 59),
(5, 5, 1, 65),
(6, 6, 1, 72),
(7, 7, 1, 73),
(8, 8, 1, 78),
(9, 9, 1, 70),
(10, 10, 1, 70),
(11, 11, 1, 63),
(12, 12, 1, 59),
(13, 1, 2, 37),
(14, 2, 2, 41),
(15, 3, 2, 36),
(16, 4, 2, 26),
(17, 5, 2, 28),
(18, 6, 2, 40),
(19, 7, 2, 30),
(20, 8, 2, 40),
(21, 9, 2, 50),
(22, 10, 2, 35),
(23, 11, 2, 30),
(24, 12, 2, 38);

-- --------------------------------------------------------

--
-- Table structure for table `peramalan`
--

CREATE TABLE `peramalan` (
  `id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `periode` varchar(255) DEFAULT NULL,
  `beta` decimal(6,1) DEFAULT NULL,
  `hasil_peramalan` decimal(6,2) DEFAULT NULL,
  `mape` decimal(6,2) DEFAULT NULL,
  `rekomendasi` decimal(6,0) DEFAULT NULL,
  `tanggal_dibuat` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `dibuat_oleh` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `peramalan`
--

INSERT INTO `peramalan` (`id`, `produk_id`, `periode`, `beta`, `hasil_peramalan`, `mape`, `rekomendasi`, `tanggal_dibuat`, `dibuat_oleh`) VALUES
(3, 1, '2021-01', '0.7', '65.80', '8.72', '66', '2021-07-17 17:01:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `harga` decimal(11,2) DEFAULT NULL,
  `stok` decimal(11,0) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `nama`, `deskripsi`, `foto`, `harga`, `stok`) VALUES
(1, 'O-Neck', 'Baju polos model O-Neck', 'kaos polos.png', '15000.00', '15'),
(2, 'Polo', 'Baju polos model polo', 'polo polos.png', '15000.00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'y',
  `last_login` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `nama`, `foto`, `role`, `is_active`, `last_login`) VALUES
(1, 'pemilik', '$2y$10$.H4P1qs.KTb2I/vGXXa8wO.B0zUZTVZyZea1.4GbezUAeQABKbUsW', 'Pemilik', 'man.png', 'pemilik', 'y', '2021-07-17 17:46:26'),
(2, 'pegawai', '$2y$10$aChAJi5ItJRB8Q.AmwXaZem5x13s7NC8vbvhNZ8x6Zx/pRc3JeqpC', 'Pegawai', 'boy.png', 'pegawai', 'y', '2021-07-17 17:37:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Penjualan` (`penjualan_id`);

--
-- Indexes for table `peramalan`
--
ALTER TABLE `peramalan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `peramalan`
--
ALTER TABLE `peramalan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD CONSTRAINT `Penjualan` FOREIGN KEY (`penjualan_id`) REFERENCES `penjualan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
