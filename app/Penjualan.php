<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Penjualan extends Model  {
    protected $table = 'penjualan';
    protected $fillable = ['tanggal', 'user_id', 'total'];
    public $timestamps = false;

    public function penjualanDetail()
    {
        return $this->hasMany(PenjualanDetail::class, 'penjualan_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(Users::class, 'user_id', 'id');
    }
}