<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Produk extends Model  {
    protected $table = 'produk';
    protected $fillable = ['nama', 'deskripsi', 'foto', 'harga', 'stok'];
    public $timestamps = false;
    protected $appends  = ["rekomendasi", "periode_rekomendasi"];

    public function getRekomendasiAttribute()
    {
        $produk_id = $this->id;
        $rekomendasi = 0;
        $peramalan = Peramalan::where("produk_id", $produk_id)->orderBy("periode", "DESC")->first();
        if ($peramalan) {
            $rekomendasi = $peramalan->rekomendasi;
        }
        return $rekomendasi;
    }

    public function getPeriodeRekomendasiAttribute()
    {
        $produk_id = $this->id;
        $periode_rekomendasi = "-";
        $peramalan = Peramalan::where("produk_id", $produk_id)->orderBy("periode", "DESC")->first();
        if ($peramalan) {
            $periode_rekomendasi = date("M Y", strtotime($peramalan->periode));
        }
        return $periode_rekomendasi;
    }
}
