<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model  {
    protected $table = 'users';
    protected $fillable = ['username', 'password', 'nama', 'foto', 'role', 'is_active', 'last_login'];
    protected $dates = ['last_login'];
    public $timestamps = false;
    protected $appends  = ["status"];

    public function getStatusAttribute()
    {
        $isActive = $this->is_active;
        $status = '';
        if ($isActive == 'y') {
            $status = 'Aktif';
        }
        if ($isActive == 'n') {
            $status = 'Non Aktif';
        }
        return $status;
    }
}
