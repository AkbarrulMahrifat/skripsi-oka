<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peramalan extends Model  {
    protected $table = 'peramalan';
    protected $fillable = [
        'produk_id',
        'periode',
        'beta',
        'hasil_peramalan',
        'mape',
        'rekomendasi',
        'tanggal_dibuat',
        'dibuat_oleh'
    ];
    public $timestamps = false;

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(Users::class, 'dibuat_oleh', 'id');
    }

    public static function getPeriode($from, $to)
    {
        $array= array();
        $month = date('Y-m', strtotime($from));
        $i = 0;
        while(date('Y-m', strtotime($month)) <= date('Y-m', strtotime($to))) {
            $array[$i] = $month;
            $month = date('Y-m', strtotime("+1 month", strtotime(date($month))));
            $i++;
        }

        return $array;
    }

    public static function getTotal($periode, $data)
    {
        $array = array();
        for($i=0; $i<count($periode); $i++) {
            for($j=0; $j<count($data); $j++) {
                if($periode[$i] == $data[$j]['periode']){
                    $array[$i] = intval($data[$j]['total']);
                    break;
                }else{
                    $array[$i] = 0;
                }
            }
        }
        return $array;
    }
}