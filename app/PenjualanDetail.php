<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PenjualanDetail extends Model  {
    protected $table = 'penjualan_detail';
    protected $fillable = ['penjualan_id', 'produk_id', 'qty'];
    public $timestamps = false;

    public function penjualan()
    {
        return $this->belongsTo(Penjualan::class, 'penjualan_id', 'id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id', 'id');
    }

    public static function scopeGetDataPenjualanPeramalan($query, $produk_id, $from, $to)
    {
        $data = $query->selectRaw('DATE_FORMAT(tanggal, "%Y-%m") as periode, sum(qty) as total')
            ->join('penjualan', 'penjualan_detail.penjualan_id', '=', 'penjualan.id')
            ->where('produk_id', $produk_id)
            ->whereRaw("DATE_FORMAT(tanggal, '%Y-%m') >= '$from'")
            ->whereRaw("DATE_FORMAT(tanggal, '%Y-%m') <= '$to'")
            ->groupBy('periode')
            ->get();

        return $data;
    }
}
