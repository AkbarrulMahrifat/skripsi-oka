<?php


namespace App\Http\Controllers;

use App\Penjualan;
use App\PenjualanDetail;
use App\Peramalan;
use App\Produk;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class PeramalanController extends Controller
{
    public function index()
    {
        $produk = Produk::all();
        $peramalan = Peramalan::with('produk')
            ->with('users')
            ->orderBy('produk_id', 'ASC')
            ->orderBy('periode', 'ASC')
            ->get();
        return view('peramalan', compact('produk', 'peramalan'));
    }

    public function hitungPeramalan(Request $request)
    {
        $this->validate($request, [
            "produk_id" => "required",
            "periode_peramalan" => "required",
        ]);

        DB::beginTransaction();
        try {
            $produk_id = $request->get('produk_id');
            $penjualan = PenjualanDetail::join('penjualan', 'penjualan_detail.penjualan_id', '=', 'penjualan.id')
                ->where('produk_id', $produk_id)
                ->orderBy('tanggal', 'asc')
                ->first();
            $from = date("Y-m", strtotime($penjualan->tanggal));
            $to = $request->get('periode_peramalan');
            $produk = Produk::find($produk_id)->first();

            $data = PenjualanDetail::getDataPenjualanPeramalan($produk_id, $from, $to);

            $periode = Peramalan::getPeriode($from, $to);

            $X = Peramalan::getTotal($periode, $data);

            $F = array();
            $e = array();
            $E = array();
            $AE = array();
            $alpha = array();
            $beta = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9];
            $PE = array();
            $MAPE = array();

            // perhitungan peramalan menggunakan nilai beta mulai dari 0.1 sampai 0.9
            for($i = 0; $i < count($beta); $i++) {
                // inisialisasi
                $F[$i][0] = $e[$i][0] = $E[$i][0] = $AE[$i][0] = $alpha[$i][0] = $PE[$i][0] = 0;
                $F[$i][1] = $X[0];
                $alpha[$i][1] = $beta[$i];

                for($j = 1; $j < count($periode); $j++){
                    // perhitungan peramalan untuk periode berikutnya
                    $F[$i][$j + 1] = ($alpha[$i][$j] * $X[$j]) + ((1 - $alpha[$i][$j]) * $F[$i][$j]);

                    // menghitung selisih antara nilai aktual dengan hasil peramalan
                    $e[$i][$j] = $X[$j] - $F[$i][$j];

                    // menghitung nilai kesalahan peramalan yang dihaluskan
                    $E[$i][$j] = ($beta[$i] * $e[$i][$j]) + ((1 - $beta[$i]) * $E[$i][$j - 1]);

                    // menghitung nilai kesalahan absolut peramalan yang dihaluskan
                    $AE[$i][$j] = ($beta[$i] * abs($e[$i][$j])) + ((1 - $beta[$i]) * $AE[$i][$j - 1]);

                    // menghitung nilai alpha untuk periode berikutnya
                    $alpha[$i][$j + 1] = $E[$i][$j] == 0 ? $beta[$i] : abs($E[$i][$j] / $AE[$i][$j]);

                    // menghitung nilai kesalahan persentase peramalan
                    $PE[$i][$j] = $X[$j] == 0 ? 0 : abs((($X[$j] - $F[$i][$j]) / $X[$j]) * 100);
                }

                $MAPE[$i] = array_sum($PE[$i])/(count($periode) - 2);
            }

            $indexPeramalan = (count($periode) - 1);
            $indexMapeTerkecil = array_search(min($MAPE), $MAPE);
            $bestBetaIndex = $beta[$indexMapeTerkecil];

            $hasil = array();
            for($j = 0; $j < count($beta); $j++) {
                for ($i = 0; $i < count($periode); $i++) {
                    if ($i < (count($periode) - 1)) {
                        $hasil[$j][$i] = [
                            'produk_id' => $produk_id,
                            'tanggal' => $periode[$i],
                            'aktual' => $X[$i],
                            'hasil' => $F[$j][$i],
                            'ek' => $e[$j][$i],
                            'Eb' => $E[$j][$i],
                            'EA' => $AE[$j][$i],
                            'alpha' => $alpha[$j][$i],
                            'beta' => $beta[$j],
                            'PE' => $PE[$j][$i],
                            'MAPE' => $MAPE[$j]
                        ];
                    } else {
                        // $nextPeriode = date('Y-m', strtotime("+1 month", strtotime(date($request->to))));
                        $hasil[$j][$i] = [
                            'produk_id' => $produk_id,
                            'tanggal' => $periode[$i],
                            'aktual' => 0,
                            'hasil' => $F[$j][$i],
                            'ek' => 0,
                            'Eb' => 0,
                            'EA' => 0,
                            'alpha' => $alpha[$j][$i],
                            'beta' => $beta[$j],
                            'PE' => 0,
                            'MAPE' => $MAPE[$j]
                        ];
                    }
                }
            }
            DB::commit();
            return view('hasil_peramalan',
                compact(
                    'hasil',
                    'bestBetaIndex',
                    'indexPeramalan',
                    'indexMapeTerkecil',
                    'beta',
                    'produk',
                    'produk_id',
                    'from',
                    'to'
                ))
                ->with('success', "Berhasil melakukan peramalan");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "produk_id" => "required",
            "periode" => "required",
            "beta" => "required",
            "hasil_peramalan" => "required",
            "mape" => "required",
            "rekomendasi" => "required"
        ]);

        DB::beginTransaction();
        try {
            $data = [
                "produk_id" => $request->input("produk_id"),
                "periode" => $request->input("periode"),
                "beta" => $request->input("beta"),
                "hasil_peramalan" => $request->input("hasil_peramalan"),
                "mape" => $request->input("mape"),
                "rekomendasi" => $request->input("rekomendasi"),
                "tanggal_dibuat" => Carbon::now(),
                "dibuat_oleh" => Session::get('id'),
            ];
            $id = [
                "produk_id" => $request->input("produk_id"),
                "periode" => $request->input("periode")
            ];
            Peramalan::updateOrCreate($id, $data);
            DB::commit();
            return redirect(Route('peramalan'))->with('success', "Berhasil menyimpan data peramalan");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $peramalan = Peramalan::where('id', $id);
            $peramalan->delete();
            DB::commit();
            return redirect()->back()->with('success', "Berhasil menghapus data");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}