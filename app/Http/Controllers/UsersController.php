<?php


namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        $users = Users::get();
        return view('users', compact('users'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nama" => "required",
            "username" => "required",
            "password" => "required",
            "role" => "required",
            "is_active" => "required",
            "foto" => "sometimes|file",
        ]);

        DB::beginTransaction();
        try {
            $data = [
                "nama" => $request->input("nama"),
                "username" => $request->input("username"),
                "password" => Hash::make($request->input("password")),
                "is_active" => $request->input("is_active"),
                "role" => $request->input("role")
            ];
            $file = $request->file('foto');
            if ($file) {
                $fileName = $file->getClientOriginalName();
                $file->move('assets/foto_profil', $file->getClientOriginalName());
                $data["foto"] = $fileName;
            }
            Users::create($data);
            DB::commit();
            return redirect()->back()->with('success', "Berhasil menambah data");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            "id" => "required",
            "nama" => "required",
            "username" => "required",
            "password" => "sometimes|nullable",
            "role" => "required",
            "is_active" => "required",
            "foto" => "sometimes|file",
        ]);

        DB::beginTransaction();
        try {
            $users = users::find($request->input("id"));
            $data = [
                "nama" => $request->input("nama"),
                "username" => $request->input("username"),
                "is_active" => $request->input("is_active"),
                "role" => $request->input("role")
            ];
            if ($request->has("password") && !is_null($request->get("password")) && !empty($request->get("password"))) {
                $data["password"] = Hash::make($request->input("password"));
            }
            $file = $request->file('foto');
            if ($file) {
                $fileName = $file->getClientOriginalName();
                $file->move('assets/foto_profil', $file->getClientOriginalName());
                $data["foto"] = $fileName;
            }
            $users->update($data);
            DB::commit();
            return redirect()->back()->with('success', "Berhasil mengubah data");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $users = Users::find($id);
            $foto = public_path().'\\assets\foto_profil\\'. $users->foto;
            if (File::exists($foto)) {
                File::delete($foto);
            }
            $users->delete();
            DB::commit();
            return redirect()->back()->with('success', "Berhasil menghapus data");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}