<?php


namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Users;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function index()
    {
        if (Session::exists('id')) {
            return redirect('/'.Session::get("role"))."/";
        } else {
            return view('login');
        }
    }

    public function login(Request $request)
    {
        $username = $request->input("username");
        $password = $request->input("password");
        // $hashed = Hash::make($password);

        $data = Users::where('username', $username)
            ->where('is_active', 'y')
            ->first();
        try {
            if (!empty($data)) {
                if ($data->is_active == "y") {
                    if (Hash::check($password, $data->password)) {
                        Session::put('id', $data->id);
                        Session::put('username', $data->username);
                        Session::put('nama', $data->nama);
                        Session::put('foto', $data->foto);
                        Session::put('role', $data->role);
                        Session::put('is_active', $data->is_active);

                        $data->update(["last_login" => Carbon::now()]);
                        return redirect($data->role.'/beranda')->with('success', "Selamat datang " . $data->nama);
                    } else {
                        throw new \Exception("Password salah.");
                    }
                } else {
                    throw new \Exception("Username tidak aktif.");
                }
            } else {
                throw new \Exception("Username tidak ditemukan.");
            }
        } catch (\Exception $ex) {
            return redirect('/login-page')->with('error', $ex->getMessage());
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('/login-page')->with('success', "Berhasil logout dari sistem");
    }
}