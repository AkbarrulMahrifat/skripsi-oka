<?php


namespace App\Http\Controllers;

use App\Penjualan;
use App\PenjualanDetail;
use App\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class PenjualanController extends Controller
{
    public function index()
    {
        $produk = Produk::all();
        $penjualan = Penjualan::with('penjualanDetail')->with('users')->get();
        return view('penjualan', compact('produk', 'penjualan'));
    }

    public function show(Request $request)
    {
        $id = $request->get('id');
        $penjualan = Penjualan::with('users', 'penjualanDetail', 'penjualanDetail.produk')
            ->where('penjualan.id', $id)
            ->first();
        return json_encode($penjualan);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "tanggal" => "required",
            "waktu" => "required",
            "items.*.produk_id" => "required",
            "items.*.qty" => "required",
        ]);

        DB::beginTransaction();
        try {
            $items = $request->input('items');
            $qty = array_column($items, 'qty');
            $total = array_sum($qty);
            $tanggalPenjualan = date("Y-m-d H:i:s", strtotime($request->input("tanggal")." ".$request->input("waktu")));
            $dataPenjualan = [
                "tanggal" => $tanggalPenjualan,
                "total" => $total,
                "user_id" => Session::get('id'),
            ];
            $penjualan = Penjualan::create($dataPenjualan);
            foreach ($items as $i) {
                $produk = Produk::where("id", $i["produk_id"])->first();
                if ($produk->stok < $i["qty"]) {
                    throw new \Exception("Qty melebihi stok produk " . $produk->nama);
                }
                $qty = $produk->stok - $i["qty"];
                $produk->update(["stok"=>$qty]);

                $dataItems = [
                    "penjualan_id" => $penjualan->id,
                    "produk_id" => $i["produk_id"],
                    "qty" => $i["qty"],
                ];
                PenjualanDetail::create($dataItems);
            }
            DB::commit();
            return redirect()->back()->with('success', "Berhasil menambah data");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $penjualan = Penjualan::where('id', $id);
            $penjualan_detail = PenjualanDetail::where("penjualan_id", $id)->get();
            foreach ($penjualan_detail as $item) {
                $produk = Produk::where("id", $item->produk_id)->first();
                $qty = $produk->stok + $item->qty;
                $produk->update(["stok"=>$qty]);
            }
            $penjualan->delete();
            DB::commit();
            return redirect()->back()->with('success', "Berhasil menghapus data");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}