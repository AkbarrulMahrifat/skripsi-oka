<?php


namespace App\Http\Controllers;

use App\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ProdukController extends Controller
{
    public function index()
    {
        $produk = Produk::all();
        return view('produk', compact('produk'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nama" => "required",
            "harga" => "required",
            "foto" => "sometimes|file",
        ]);

        DB::beginTransaction();
        try {
            $data = [
                "nama" => $request->input("nama"),
                "harga" => $request->input("harga"),
                "deskripsi" => $request->input("deskripsi")
            ];
            $file = $request->file('foto');
            if ($file) {
                $fileName = $file->getClientOriginalName();
                $file->move('assets/foto_produk', $file->getClientOriginalName());
                $data["foto"] = $fileName;
            }
            Produk::insert($data);
            DB::commit();
            return redirect()->back()->with('success', "Berhasil menambah data");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            "id" => "required",
            "nama" => "required",
            "harga" => "required",
            "foto" => "sometimes|file",
        ]);

        DB::beginTransaction();
        try {
            $produk = Produk::find($request->input("id"));
            $data = [
                "nama" => $request->input("nama"),
                "harga" => $request->input("harga"),
                "deskripsi" => $request->input("deskripsi")
            ];
            $file = $request->file('foto');
            if ($file) {
                $fileName = $file->getClientOriginalName();
                $file->move('assets/foto_produk', $file->getClientOriginalName());
                $foto_old = public_path().'\\assets\foto_produk\\'. $produk->foto;
                if (File::exists($foto_old)) {
                    File::delete($foto_old);
                }
                $data["foto"] = $fileName;
            }
            $produk->update($data);
            DB::commit();
            return redirect()->back()->with('success', "Berhasil mengubah data");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $produk = Produk::find($id);
            $foto = public_path().'\\assets\foto_produk\\'. $produk->foto;
            if (File::exists($foto)) {
                File::delete($foto);
            }
            $produk->delete();
            DB::commit();
            return redirect()->back()->with('success', "Berhasil menghapus data");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function stok()
    {
        $produk = Produk::all();
        return view('stok', compact('produk'));
    }

    public function updateStok(Request $request)
    {
        $this->validate($request, [
            "id" => "required",
            "stok" => "required",
        ]);

        DB::beginTransaction();
        try {
            $produk = Produk::find($request->input("id"));
            $data = [
                "stok" => $request->input("stok"),
            ];
            $produk->update($data);
            DB::commit();
            return redirect()->back()->with('success', "Berhasil mengubah data");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}